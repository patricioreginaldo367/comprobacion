<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DateController extends Controller
{
    public function date()
    {
        $fecha = date("Y-m-d");
        return view('date', compact('fecha'));
    }
}